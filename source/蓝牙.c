/*------------------------------------------------------------------*/
/* --- STC MCU Limited ---------------------------------------------*/
/* --- STC10/11xx Series MCU UART (8-bit/9-bit)Demo ----------------*/
/* --- Mobile: (86)13922805190 -------------------------------------*/
/* --- Fax: 86-755-82905966 ----------------------------------------*/
/* --- Tel: 86-755-82948412 ----------------------------------------*/
/* --- Web: www.STCMCU.com -----------------------------------------*/
/* If you want to use the program or the program referenced in the  */
/* article, please specify in which data and procedures from STC    */
/*------------------------------------------------------------------*/


#include <STC11F60XE.h>
#include "intrins.h"

sbit P10 = P1^0;
sbit P11 = P1^1;
sbit P12 = P1^2;
sbit P13 = P1^3;
sbit P14 = P1^4;
sbit P15 = P1^5;
sbit P16 = P1^6;
sbit P17 = P1^7;


char xx[20];
char xnum = 0;

void Delay10ms()		//@12.000MHz
{
	unsigned char i, j;

	_nop_();
	_nop_();
	i = 117;
	j = 183;
	do
	{
		while (--j);
	} while (--i);
}


void Delay100ms()		//@12.000MHz
{
	unsigned char i, j, k;

	i = 5;
	j = 144;
	k = 71;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}


void setp1(char num,char state)
{
	num = num-48;
	if(state == 0)
	{
		switch(num){
			case 1:P10=0;break;
			case 2:P11=0;break;
			case 3:P12=0;break;
			case 4:P13=0;break;
			case 5:P14=0;break;
			case 6:P15=0;break;
			case 7:P16=0;break;
			case 8:P17=0;break;
			default:break;
		}
	}
	if(state == 1)
	{
		switch(num){
			case 1:P10=1;break;
			case 2:P11=1;break;
			case 3:P12=1;break;
			case 4:P13=1;break;
			case 5:P14=1;break;
			case 6:P15=1;break;
			case 7:P16=1;break;
			case 8:P17=1;break;
			default:break;
		}
	}
}

void UartInit(void)		//9600bps@12.000MHz
{
	PCON &= 0x7F;		//波特率不倍速
	SCON = 0x50;		//8位数据,可变波特率
	AUXR |= 0x04;		//独立波特率发生器时钟为Fosc,即1T
	BRT = 0xD9;		//设定独立波特率发生器重装值
	AUXR |= 0x01;		//串口1选择独立波特率发生器为波特率发生器
	AUXR |= 0x10;		//启动独立波特率发生器
}

void main()
{
	UartInit();
	ES = 1;                 //Enable UART interrupt
	EA = 1;                 //Open master interrupt switch
	
	P1 = 0xFf;
	while(1);
}

/*----------------------------
UART interrupt service routine
----------------------------*/
void Uart_Isr() interrupt 4 using 1
{
	char rbuff;
    if (RI)
    {
      
		rbuff = SBUF;
		if(rbuff == '@')
			xnum = 1;
		xx[xnum++] = rbuff;
		if(rbuff == '#')
		{
		
			if(xx[6] == 'N')
				setp1(xx[3],0);
			if(xx[6] == 'F')
				setp1(xx[3],1);
			xnum = 0;

		}

		if(xnum>10)
			xnum = 0;

		RI = 0;             //Clear receive interrupt flag

    }
    if (TI)
    {
        TI = 0;             //Clear transmit interrupt flag
    }
}


